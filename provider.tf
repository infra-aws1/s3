terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.6.2"
    }
  }
}
provider "aws" {
  region = var.region
  # Tags default
  default_tags {
    tags = {
      Owner     = "Leopoldo Cardoso"
      Managed   = "Terraform"
      objective = "Estudos IAC"
    }
  }
}