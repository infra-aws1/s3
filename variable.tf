variable "region" {
  type        = string
  description = "Região default de deploy dos nossos recursos"
  default     = "us-east-1"
}