resource "aws_s3_bucket" "bucket-terraform" {
  bucket = "iacterraformtfstate-01"
  lifecycle {
    prevent_destroy = true
  }
}