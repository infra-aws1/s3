# Bucket S3

 Criação de um Bucket S3 através de automação utilizando Terraform.

 Este bucket tem como objetivo guardar os arquivos de estado do terraform.

 O arquivo provider.tf contém configurações do provider e tags default.

 Para criação deste bucket utilizei variáveis que estão no arquivo variable.tf.

 Na configuração do bucket utilizei o meta argument lifecycle **prevent_destroy = true** que evita a exclusão acidental do bucket.

 O arquivo outputs.tf mostra informações do recursos após a sua criação, neste exemplo o arn do S3. Este arquivo é interessante para conferência do recurso criado sem necessidade de acessar o console da aws.

 Caso siga este exemplo para criar seu bucket s3 lembre-se de alterar o nome, pois o mesmo deve ser único.

 Para criação dos recursos via terraform, necessário rodar os seguintes comandos após logar na AWS:

 1 - terraform init: inicializar o terraform

 2 - terraform validate: validar o código do terraform (opcional)

 3 - terraform fmt: ajusta a identação do código (opcional)

 4 - terraform plan: planeja os recursos que serão criados com saída na tela

 5 - terraform apply: aplica o código para criação dos recursos. Necessário confirmar com yes
 
 6 - terraform destroy: destroi os recursos criados. Necessário confirmar com yes